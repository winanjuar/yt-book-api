import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entities/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserRepository extends Repository<User> {
  constructor(@InjectRepository(User) repository: Repository<User>) {
    super(repository.target, repository.manager, repository.queryRunner);
  }

  async createUser(user: CreateUserDto): Promise<User> {
    const newUser = this.create();
    newUser.name = user.name;
    newUser.email = user.email;
    newUser.salt = await bcrypt.genSalt();
    newUser.password = await bcrypt.hash(user.password, newUser.salt);

    try {
      return await this.save(newUser);
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException(`email ${user.email} is already used`);
      } else {
        throw new InternalServerErrorException(
          'Something wrong when create new user',
        );
      }
    }
  }

  async validateUser(email: string, password: string): Promise<User> {
    const user = await this.findOneBy({ email });
    if (user && (await user.validatePassword(password))) {
      return user;
    }
    return null;
  }
}
