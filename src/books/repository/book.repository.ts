import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBookDTO } from '../dto/create-book.dto';
import { FilterBookDTO } from '../dto/filter-book.dto';
import { UpdateBookDTO } from '../dto/update-book.dto';
import { Book } from '../entity/book.entity';

@Injectable()
export class BookRepository extends Repository<Book> {
  private readonly logger = new Logger(BookRepository.name);

  constructor(
    @InjectRepository(Book)
    repository: Repository<Book>,
  ) {
    super(repository.target, repository.manager, repository.queryRunner);
  }

  async createBook(book: CreateBookDTO): Promise<Book> {
    const newBook = this.create(book);
    try {
      return await this.save(newBook);
    } catch (error) {
      this.logger.error('Something wrong when create a book');
      throw new InternalServerErrorException(error);
    }
  }

  async getBookId(id: string): Promise<Book> {
    const book = await this.findOneBy({ id });
    if (!book) {
      this.logger.warn('Book not found');
      throw new NotFoundException(`Book with id ${id} not found`);
    }
    return book;
  }

  async updateBook(id: string, newBook: UpdateBookDTO): Promise<Book> {
    const book = await this.getBookId(id);
    try {
      return await this.save({ ...book, ...newBook });
    } catch (error) {
      this.logger.error('Something wrong when update a book');
      throw new InternalServerErrorException(error);
    }
  }

  async deleteBook(id: string): Promise<Book> {
    const book = await this.getBookId(id);
    try {
      return await this.remove(book);
    } catch (error) {
      this.logger.error('Something wrong when delete a book');
      throw new InternalServerErrorException(error);
    }
  }

  async whereFilter(filter: FilterBookDTO): Promise<Book[]> {
    const { title, author, category, min_year, max_year } = filter;
    const query = this.createQueryBuilder('book');
    if (title) {
      query.andWhere('book.title ILIKE :title', { title: `%${title}%` });
    }
    if (author) {
      query.andWhere('book.author ILIKE :author', { author: `%${author}%` });
    }
    if (category) {
      query.andWhere('book.category ILIKE :category', {
        category: `%${category}%`,
      });
    }
    if (min_year) {
      query.andWhere('book.year >= :min_year', { min_year });
    }
    if (max_year) {
      query.andWhere('book.year <= :max_year', { max_year });
    }
    return await query.getMany();
  }
}
