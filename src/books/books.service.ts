import { Injectable, Logger } from '@nestjs/common';
import { CreateBookDTO } from './dto/create-book.dto';
import { FilterBookDTO } from './dto/filter-book.dto';
import { UpdateBookDTO } from './dto/update-book.dto';
import { Book } from './entity/book.entity';
import { BookRepository } from './repository/book.repository';

@Injectable()
export class BooksService {
  private readonly logger = new Logger(BooksService.name);
  constructor(private bookRepository: BookRepository) {}

  async getBooks(filter: FilterBookDTO): Promise<Book[]> {
    this.logger.log('Returned filtered book');
    return await this.bookRepository.whereFilter(filter);
  }

  async createBook(book: CreateBookDTO): Promise<Book> {
    this.logger.log('Created a book');
    return await this.bookRepository.createBook(book);
  }

  async updateBook(id: string, newBook: UpdateBookDTO): Promise<Book> {
    this.logger.log('Updated a book');
    return await this.bookRepository.updateBook(id, newBook);
  }

  async deleteBook(id: string): Promise<Book> {
    this.logger.log('Deleted a book');
    return await this.bookRepository.deleteBook(id);
  }

  async getBookId(id: string): Promise<Book> {
    this.logger.log(`Returned a book with id ${id}`);
    return this.bookRepository.getBookId(id);
  }

  // private books: any[] = [];
  // getBooks(filter: FilterBookDTO): any[] {
  //   const { title, author, category, min_year, max_year } = filter;
  //   const books = this.books.filter((book) => {
  //     if (title && !book.title.includes(title)) {
  //       return false;
  //     }
  //     if (author && !book.author.includes(author)) {
  //       return false;
  //     }
  //     if (category && !book.category.includes(category)) {
  //       return false;
  //     }
  //     if (min_year && book.year < min_year) {
  //       return false;
  //     }
  //     if (max_year && book.year > max_year) {
  //       return false;
  //     }
  //     return true;
  //   });
  //   return books;
  // }
  // getABook(id: string): any {
  //   const bookIdx = this.findBookById(id);
  //   return this.books[bookIdx];
  // }
  // createNewBook(createBookDTO: CreateBookDTO): void {
  //   const { title, author, category, year } = createBookDTO;
  //   this.books.push({ id: uuidv4(), title, author, category, year });
  // }
  // updateBook(id: string, updateBookDTO: UpdateBookDTO): void {
  //   const bookIdx: number = this.findBookById(id);
  //   const { title, author, category, year } = updateBookDTO;
  //   this.books[bookIdx].title = title;
  //   this.books[bookIdx].author = author;
  //   this.books[bookIdx].category = category;
  //   this.books[bookIdx].year = year;
  // }
  // deleteBook(id: string): void {
  //   const bookIdx = this.findBookById(id);
  //   this.books.splice(bookIdx, 1);
  // }
  // findBookById(id: string): number {
  //   const bookIdx = this.books.findIndex((book) => book.id === id);
  //   if (bookIdx === -1) {
  //     throw new NotFoundException(`Book with id ${id} is not found`);
  //   }
  //   return bookIdx;
  // }
}
