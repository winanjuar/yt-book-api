// import { Type } from 'class-transformer';
// import { IsInt, IsNotEmpty } from 'class-validator';
// export class UpdateBookDTO {
//   @IsNotEmpty()
//   title: string;

import { PartialType } from '@nestjs/mapped-types';
import { CreateBookDTO } from './create-book.dto';

//   @IsNotEmpty()
//   author: string;

//   @IsNotEmpty()
//   category: string;

//   @IsNotEmpty()
//   @IsInt()
//   @Type(() => Number)
//   year: number;
// }

export class UpdateBookDTO extends PartialType(CreateBookDTO) {}
