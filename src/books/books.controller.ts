import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { UUIDValidationPipe } from 'src/pipes/uuid-validation.pipe';
import { BooksService } from './books.service';
import { CreateBookDTO } from './dto/create-book.dto';
import { FilterBookDTO } from './dto/filter-book.dto';
import { UpdateBookDTO } from './dto/update-book.dto';

@Controller('books')
export class BooksController {
  constructor(private booksService: BooksService) {}

  @Get()
  async getBooks(@Query() filter: FilterBookDTO) {
    return await this.booksService.getBooks(filter);
  }

  @Get('/:id')
  async getABook(@Param('id', ParseUUIDPipe) id: string) {
    return await this.booksService.getBookId(id);
  }

  @Post()
  async createNewBook(@Body() createBookDTO: CreateBookDTO) {
    return await this.booksService.createBook(createBookDTO);
  }

  @Put('/:id')
  async updateBook(
    @Param('id', UUIDValidationPipe) id: string,
    @Body() payload: UpdateBookDTO,
  ) {
    return await this.booksService.updateBook(id, payload);
  }

  @Delete('/:id')
  async deleteBook(@Param('id', ParseUUIDPipe) id: string) {
    return await this.booksService.deleteBook(id);
  }
}
