import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { RefreshToken } from '../entity/refresh-token.entity';

@Injectable()
export class RefreshTokenRepository extends Repository<RefreshToken> {
  constructor(
    @InjectRepository(RefreshToken) repository: Repository<RefreshToken>,
  ) {
    super(repository.target, repository.manager, repository.queryRunner);
  }

  async createRefreshToken(user: User, ttl: number): Promise<RefreshToken> {
    const refreshToken = this.create();
    refreshToken.user = user;
    refreshToken.isRevoked = false;
    const expiredAt = new Date();
    expiredAt.setSeconds(expiredAt.getSeconds() + ttl);
    refreshToken.expiredAt = expiredAt;

    return await this.save(refreshToken);
  }

  async findOneByJid(id: string): Promise<RefreshToken> {
    return await this.findOne({
      where: { id },
      relations: ['user'],
    });
  }
}
