import {
  BadRequestException,
  Injectable,
  Logger,
  PipeTransform,
} from '@nestjs/common';
import { isUUID } from 'class-validator';

@Injectable()
export class UUIDValidationPipe implements PipeTransform {
  private readonly logger = new Logger(UUIDValidationPipe.name);
  transform(value: any) {
    if (!isUUID(value, 4)) {
      this.logger.error('Not valid UUID');
      throw new BadRequestException(`Value ${value} is not valid UUID`);
    }
    return value;
  }
}
