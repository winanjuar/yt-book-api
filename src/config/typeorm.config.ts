import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 54321,
  username: 'postgres',
  password: 'S3cret',
  database: 'book_api',
  entities: [__dirname + '/../**/*.entity.{ts,js}'],
  synchronize: true,
};
